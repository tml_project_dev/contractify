import React from 'react';
import LoginForm from './forms/loginform';

const LoginPage = () =>  {
    return (
        <div className="login_page_container">
            <label className="label form_title_login">Log In</label>
            <LoginForm/>
        </div>
        
    );

}

export default LoginPage;