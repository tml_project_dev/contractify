import React, { Component } from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom' 

class RegisterForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userid : {
                value : '',
                type: String,
                validation : false

            },
            name : {
                value : '',
                type: String,
                validation : false
            },
            password : {
                value : '',
                type: String,
                validation : true
            },
            confirmpassword: {
                value : '',
                type: String,
                validation : true
            },
            title : {
                value : '',
                type: String,
                validation : false
            }
            
        }

        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.onSubmitHandler = this.onSubmitHandler.bind(this);
    }
    onChangeHandler () {
        
    }

    onSubmitHandler (props) {
        console.log("Form Submitting");
        const user_id = document.querySelector("#id_field").value;
        const password = document.querySelector("#password_field").value;
        const confirmed_password = document.querySelector("#confpassword_field").value;
        const name = document.querySelector("#name_field").value;
        const title = document.querySelector("#title_field").value;
        console.log(user_id, password,confirmed_password,title,name);
        if(!(password===confirmed_password)) {
            console.log('Paswwords do not match');
            return;
        }
        console.log(this.props.history);
        axios.post('/registerlogin/register', {
            name : name,
            userid : user_id,
            password : password,
            title : title
        }).then(response  => {
            console.log(response);
            if (response.data.loginSuccess) {
                this.props.history.push('/dashboard');
            }
        });

    }

    render() {
        return (
            <div className="register_form">
                    <div className="field">
                        <label className="label is-primary">User ID:</label>
                        <div className="control has-icons-left has-icons-right">
                            <input className="input is-primary" type="text" placeholder="Enter UserID" id="id_field" />
                            <span className="icon is-small is-left">
                            <i class="fas fa-address-card"></i>
                            </span>
                            <span className="icon is-small is-right">
                            <i className="fas fa-check"></i>
                            </span>
                        </div>   
                    </div>
                    <div className="field">
                        <label className="label is-primary">Full Name:</label>
                        <p className="control has-icons-left">
                            <input className="input is-primary" type="text" placeholder="Enter Name" id="name_field"/>
                            <span className="icon is-small is-left">
                            <i className="fas fa-user"></i>
                            </span>
                        </p>
                    </div>
                   
                    <div className="field">
                        <label className="label is-primary">Password:</label>
                        <p className="control has-icons-left">
                            <input className="input is-primary" type="password" placeholder="Password" id="password_field"/>
                            <span className="icon is-small is-left">
                            <i className="fas fa-lock"></i>
                            </span>
                        </p>
                    </div>
                    <div className="field">
                        <label className="label is-primary">Confirm Password:</label>
                        <p className="control has-icons-left">
                            <input className="input is-primary" type="password" placeholder="Confirm Password" id="confpassword_field"/>
                            <span className="icon is-small is-left">
                            <i className="fas fa-lock"></i>
                            </span>
                        </p>
                    </div>
                    <div className="field">
                        <label className="label is-primary">Title:</label>
                        <p className="control has-icons-left">
                            <input className="input is-primary" type="text" placeholder="Enter Title" id="title_field"/>
                            <span className="icon is-small is-left">
                            <i class="fas fa-users-cog"></i>
                            </span>
                        </p>
                    </div>
                    <div className="loginbtn_container">
                        <div className="field">
                            <p className="control">
                                <button className="button is-primary is-rounded" onClick={this.onSubmitHandler}>
                                Register
                                </button>
                            </p>
                        </div>
                    </div>
                </div>
        );
    }
}

export default RegisterForm;