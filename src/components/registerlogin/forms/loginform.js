import React, { Component } from 'react';

import axios from 'axios';
import { withRouter } from 'react-router-dom';




class LoginForm extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            loading : false
        }
        this.handleSumbitLogin = this.handleSumbitLogin.bind(this);

    }

    handleSumbitLogin() {
        console.log("Submitting form : Login");
        const user_id = document.querySelector("#id_field").value;
        const password = document.querySelector("#password_field").value;
        axios.post('/registerlogin/login', {
            userid : user_id,
            password : password,
        }).then(response => {
            console.log(response);
            if (response.data.loginSuccess) {
                this.props.history.push('/dashboard');
            }
        });
    }

    render() {
        return (
                <div className="login_form">
                    <div className="field">
                        <label className="label is-primary">User ID:</label>
                        <div className="control has-icons-left has-icons-right">
                            <input className="input is-primary" type="text" placeholder="Enter UserID" id="id_field"/>
                            <span className="icon is-small is-left">
                            <i className="fas fa-user"></i>
                            </span>
                            <span className="icon is-small is-right">
                            <i className="fas fa-check"></i>
                            </span>
                        </div>   
                    </div>
                    <div className="space"></div>
                    <div className="field">
                        <label className="label is-primary">Password:</label>
                        <p className="control has-icons-left">
                            <input className="input is-primary" type="password" placeholder="Password" id="password_field"/>
                            <span className="icon is-small is-left">
                            <i className="fas fa-lock"></i>
                            </span>
                        </p>
                    </div>
                    <div className="loginbtn_container">
                        <div className="field">
                            <p className="control">
                                <button className="button is-primary is-rounded" onClick={this.handleSumbitLogin}>
                                Login
                                </button>
                            </p>
                        </div>
                    </div>
                </div>
        );
    }
}

export default LoginForm;