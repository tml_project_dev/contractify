import React from 'react';
import RegisterForm from './forms/registerform';

const RegisterPage = () => {
    return (
        <div className="register_page_container">
            <label className="label form_title_register">
                Register
            </label>
            <RegisterForm />
        </div>
    );
    
}

export default RegisterPage;