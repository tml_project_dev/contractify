import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import TataMotorsLogo from '../../resources/images/tata_motors_logo.png';
import TataLogo from '../../resources/images/tata_logo.png';

const Header = () => {
    return (
        <AppBar style={{backgroundColor:"white", display:'flex', flexDirection:'row', justifyContent: 'space-between'}}>
            <div className="header_left">
                <div className="tata_motors_logo">
                    <img src={TataMotorsLogo}></img>
                </div>
                <label className="logo_label_left">
                    CONTRACTIFY
                </label>
            </div>
            <div className="tata_logo_header_right">
                <img className="tata_logo_img" src={TataLogo}></img>
            </div>
            
        </AppBar>
    );
};

export default Header;