import React, { Component } from 'react';
import DatePicker from "react-datepicker";
 
import "react-datepicker/dist/react-datepicker.css";


class Contractify extends Component {
    constructor(props) {
        super(props);

        this.state = {
            db : {
                services_agreement : {
                    sections : [
                        'SCOPE OF SERVICES AND OBLIGATIONS OF THE SERVICE PROVIDER',
                        'PAYMENTS & CHARGES',
                        'TERM & TERMINATION',
                        'OTHER TERMS AND CONDITIONS'
                    ],
                    data : {
                        ss : ['During the term and subject to the terms and conditions of this Agreement, the Service Provider undertakes to provide the Services as more specifically mentioned in Annexure-I.'],
                        pc : [
                            'For the Services provided in a month, the Service Provider shall raise the bill and submit the invoice in respect thereof by the 5 th day of the next month. TML shall verify the bills submitted by the Service Provider and release the payments within 30 days from the date of submission of bills. However, if, upon verification by TML, the bill is found to be incorrect or inappropriate, the same shall be returned by TML to the Service Provider for correction and revision. The Service Provider shall thereafter submit the revised bill which shall be verified by TML and payments shall be released within 30 days of submission of bills.',
                            'Service Tax shall be payable extra, as applicable. The Service Provider shall, in the invoice raised on TML, separately show Service Tax payable on the Services rendered and shall also show other necessary details such as service tax registration no. etc. so as to enable TML to claim credit for the same as per law. The Service Provider shall, further, every month or as and when demanded by TML, provide all the necessary documentary proof evidencing payment of Service Tax and/or any other tax by the Service Provider to the Government in respect of the Services rendered to TML under this Agreement.',
                            'Payment will be subject to Service Provider providing TML proof of payment of Service Provider‘s statutory dues such as contributions made towards Provident Fund, ESI and Gratuity etc in respect of the employees/ personnel employed by it at “Business Premises” and observance of other statutory compliances.',
                            'No other costs, payments and expenses would be borne by TML unless specifically mentioned in this agreement or mutually agreed in writing in advance.'
                        ],
                        tt : ['This Agreement shall be effective with effect from ____________ and shall be valid for a period of _________ i.e. till ____________. The term of this agreement may further be extended for such period and on such terms as the parties may mutually decide. This Agreement shall come to an end upon the expiry of its Term and/or the renewal period thereof.'],
                        otc : []
                    }
                },
                transport_agreement : {}

            },
            contract : {
                bw1 : '',
                bw2 : '',
                valid_from : new Date(),
                duration : '',
                type:'',
                sections : [],
                clauses :{}
            },
            initialFlag:true
        }
        this.startContract = this.startContract.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.resetContract = this.resetContract.bind(this);
        this.renderSections = this.renderSections.bind(this);
        this.proposeClauses = this.proposeClauses.bind(this);
        this.handleCheckboxChange = this.handleCheckboxChange(this);
    }

    handleCheckboxChange() {
        console.log('A checkbo was tapped');
    }

    proposeClauses() {

    }
    renderSections() {
        console.log('Called');
        console.log(this.state.contract.sections);
    }
    resetContract() {
        this.setState({
            initialFlag:true
        })
    }
    startContract() {
        console.log("Start Contract");
        const initialData = {
            type : document.getElementById("field_contract_type").value,
            bw1 : document.getElementById("field_bw1").value,
            bw2 : document.getElementById("field_bw2").value,
            valid_from : this.state.contract.valid_from,
            duration : document.getElementById('duration-input').value + ' ' +document.getElementById('field-contract-duration').value,
            sections : [],
            clauses : {}

        }
        if(initialData.type === 'Service Agreement') {
            initialData.sections = this.state.db.services_agreement.sections;
            initialData.clauses = this.state.db.services_agreement.data;
        }
        this.setState({
            contract : initialData,
            initialFlag : false
        });
        
    }
    handleChangeDate(event) {
        console.log("Date Changed", event);
    }
    render() {
        if(this.state.initialFlag)
            return (
                <div>
                    <div className="control">
                        <label>Contract Type:                         </label>
                        <div className="select">
                            <select id="field_contract_type">
                            <option defaultValue>Select Contract Type</option>
                            <option value='Service Agreement'>Service Agreement</option>
                            <option value='Transportation Agreement'>Transportation Agreement</option>
                            </select>
                        </div>
                        <hr>
                        </hr>
                        <label>Contract Signed Between: </label>
                        <div className="field">
                            <div className="control">
                                <div className='adressee_container'>
                                    <input className="input is-primary" type="text" placeholder="" id="field_bw1"/>
                                    <p className="and_span">&</p>
                                    <input className="input is-primary" type="text" placeholder="" id="field_bw2"/>
                                </div>
                                <label>Contract Valid From:</label>
                                <br/>
                                <DatePicker
                                    className="input is-primary"
                                    selected={this.state.contract.valid_from}
                                    onChange={this.handleChangeDate}
                                />
                                <div>
                                    <label>Valid For: </label>
                                </div>
                                    
                                <div className="row">
                                    <input className="input is-primary" id="duration-input"/>
                                    <div className="select">
                                        <select id="field-contract-duration">
                                            <option value='Days'>Days</option>
                                            <option value='Months' defaultValue>Months</option>
                                            <option value='Years'>Years</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="start-btn-container">
                                    <button className="button is-rounded is-primary start-contract-button" onClick={this.startContract}>Next</button>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            );
        else {
            const sections_list = this.state.contract.sections;
            console.log(sections_list)
            return(
                <div>
                    <div className="contract-title">
                        <label>{this.state.contract.type}</label>
                    </div>
                    <hr></hr>
                    {this.state.contract.sections.map((section, i)=> {
                        return <div className="sections-list" key={i}>
                            <input type="checkbox" onChange={(event)=>{
                                console.log(event.target);
                            }}/>
                            <span contentEditable>{section}</span>
                        </div>
                    })}
                    <div className="propose-clauses-btn-container">
                        <button className="button is-rounded is-primary propose-clauses-button" onClick={this.proposeClauses}>Next</button>
                    </div>
                </div>
            );
        }
        
    }
}

export default Contractify;