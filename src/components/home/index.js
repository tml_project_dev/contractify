import React, { Component } from 'react';
import Contractify from './contractify';

class Home extends Component {
    componentDidMount() {
        console.log("Dashboard");
    }
    render() {
        return (
            <div className="contractify-container">
                <Contractify />
            </div>
        );
    }
}

export default Home;