import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Layout from './hoc/layout';
import Home from './components/home';
import RegisterPage from './components/registerlogin/register';
import LoginPage from './components/registerlogin/login';

const Routes = () => {
    return (
      <Layout>
        <Switch>
          <Route path="/a" exact component={Home}/>
          <Route path="/register" exact component={RegisterPage}/>
          <Route path="/login" exact component={LoginPage}/>
        </Switch>
      </Layout>
      
    );
  
}

export default Routes;
