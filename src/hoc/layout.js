import React, { Component } from 'react';

import Header from '../components/header/header';

class Layout extends Component {
    render() {
        return (
            <div>
                <Header/>
                <div className="page_container">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default Layout;